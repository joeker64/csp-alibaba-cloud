# Setup of Alibaba Cloud for ARM-CSP architecture


## Setting up RAM account
A resource access management (RAM) is needed to provide an account for your listed devices/services. Permissions also need to be set up for this RAM account to ensure that your devices/servies will not produce a permission error.

### Prerequisite
- Created an "Alibaba cloud account"

### Create a RAM account
1. Sign-in to your Alibaba cloud account and head to "console" page
2. Head to the "resource access management" page. This can be done by using the search bar located at the top-right of the page
3. On the left side of the page, head to the "users" sub-page which is located under "identities". 
4. Create your RAM account, Note: ensure your save the access & secret keys with this RAM user as this will be used for authentication

### Setting up permissions of RAM account
1. On the same page above, click on the RAM user you've just created
2. Click onto the permissions tab and click "grant permission". Add these permissions to enable the RAM user to run the "Traffic Monitoring" application:
    - AdministratorAccess
    - AliyunOSSFullAccess
    - AliyunECSFullAccess
    - AliyunRDSFullAccess
    - AliyunFCFullAccess
    - AliyunEventBridgeFullAccess
    - AliyunCloudMonitorFullAccess

## Setup Link IoT Edge service
Follow this guide or for more information head into the "docs" directory

### Prerequisite
- Arm based Embedded Platform
    Tested:
        - Rockchip 3399 Pro D
        - NXP IMX 8M Plus
- Built all "Application Microservices" containers on device
- Alibaba cloud account with RAM account set up

### Create an Edge instance on Link IoT Edge console and adding properties to the device
1. Sign-in to your Alibaba cloud account and head to "console" page
2. Head to the "Link Iot Edge". This can be done by using the search bar located at the top-right of the page
    - Note: If you are having any issues change the region to "Singapore"
3. On the "Link Iot Edge" page, click on "Edge instances" on the left side menu and "create instance"
4. Provide any instance name preferred name would be the name of the device which the Link IoT Edge is being deployed.
5. For Gateway Products select Create Gateway product and provide the product name preferred to be same as the gateway name and click on Ok.
6. For Gateway devices click on Add Gateway Device and Provide the device name. This name is used in IoT platform for getting credentials.
7. In the instance type dropdown select standard edition and click on ok
8. Open Alibaba Cloud console in another tab in browser and search for IoT platform and select IoT platform from search results.
9. In the left-hand side bar select products and we can see a new product with the product name given previously has been created.
10. Click on the newly created product and in the product, console click on Define feature from the top menu and click on "Edit draft".
12. In this window, we have to add a feature for each of the detection parameters.
13. The table below describes the features that we have to add and their configuration:


| Feature name   | Identifier     | Data type |
|----------------|----------------|-----------|
| person         | person         | int32     |
| car            | car            | int32     |
| bus            | bus            | int32     |
| truck          | truck          | int32     |
| bicycle        | bicycle        | int32     |
| end_to_end_fps | end_to_end_fps | float     |
| pure_fps       | pure_fps       | float     |
| anl_res        | anl_res        | text      |

14. After adding all the features click on release online button at bottom and
check the box and click on ok to save the changes.

### Creating function on Function Compute and adding our lambda function 
1. Go to the Alibaba console and search for Function compute and navigate to the Function compute console.
2. Click on Services and Functions on the left side menu.
3. Click on create service, provide a service name and click on ok.
4. On the next window click on Create Function and provide one sample function name, select runtime as python3.6 and keep all the other options as default and click on create.
5. In the project folder open the python file located at Link_IoT_Edge/index.py in an editor.
6. Go to the Alibaba IoT Platform console and select devices from the left side menu.
7. We can see one device has been created, click on the device and on the top side we can see details about the products, Product key and DeviceSecret.
8. Click on view for the DeviceSecret entry and copy the device name and product key.
9. Update the product key and device name in index.py at line number 14, 15, 16 and save the script.
10. In the index.py file update Alibaba access key, secret key in the line no. 5
11. Save the script and compress the modified script in the zip format.
12. In the Alibaba Function Compute console navigate to the function which we created and on the top side select upload zip package from upload code dropdown.
13. Upload the zip file which we created.
14. This will load our function.
15. Select trigger in the top menu and select Create Trigger.
16. In the dropdown search for EventBridge and select it.
17. Enter a name for the trigger and click on Ok.
18. In the top search bar of Alibaba console search for Event bridge and navigate to event bridge console.
19. On the left side menu select Event buses and click on default event bus
20. On left side menu select Event rules and we can see a new event rule with the trigger name given in function compute console has been created.
21. Click on the event rule and on top right select Edit Pattern.
22. Copy the contents of the config file located in Link_IoT_Edge/event_rule.json and replace the content in the Edit Pattern with the copied configuration and click on ok.

### Installing Gateway software on the device and Deploying the Edge instance from cloud
1. Go to the target device terminal(Rockchip RK3399 ProD or NXP Imx8MP device terminal) and execute the following command to download the Link IoT Edge package for aarch64:
```
wget http://link-iot-edge-packet.oss-cn-shanghai.aliyuncs.com/aarch64-linux-gnu/link-iot-edge-aarch64-v2.7.0.tar.gz
```
2. Execute the following command to install the python dependencies:
```
python3 -m pip install ws4py paho-mqtt --index-url https://mirrors.aliyun.com/pypi/simple/
```
3. Execute the following command to extract the downloaded archive to root directory:
```
tar -xvf link-iot-edge-aarch64-v2.7.0.tar.gz -C /
```
4. Go to Alibaba console and navigate to IoT Platform console
5. On the left side select Devices and select the device which was created previously
6. On the top side, select view for the DeviceSecret and copy the product key, device name, device secret key for further steps.
7. In the device window click on here for Mqtt connection parameters and copy the Mqtt host url and note it down for further steps.
8. To start the Link IoT Edge service we need to provide a command in the following format:
```
/linkedge/gateway/build/bin/linkedged <Product_key> <Device_name> <Device_secret_key> <Product_MQTT_host_url> 1883 0 backend-iot-edge-tunnel-singapore.aliyun.com 443 1
```
9. See the status of the Link IoT Edge using the following command:
```
/linkedge/gateway/build/bin/linkedged status
```
### Setup the Apsara Live service for live streaming
- Follow document Cloud_container/docs/ARM-Smart_camera_CSP_Alibaba_Setting_up_Apsara_Live_and_Apsara_VOD_service.pdf for setup of Apsara Live service and getting the Live streaming ingest and pull URL.

### Build the cloud container
1. Enter the "cloud_container/config" directory. Here you will see three configurations files which you will need to update
2. In "alibaba_creds.json", enter your region ID and your RAM user's access key and secret key
3. In "live_recording_config.json", enter the data you used for the Apsara live service. This is optional and if you have no set up a Apsara service then leave the file as it
4. In "service_config.json", enter your devices product key and device name which were created when setting up an IoT device in alibaba cloud. For "cloud_monitor_group_id" create a group id which can be any number e.g 1.
5. Go back to the "cloud_container" directory and build the container using this command. Change the name to what you would like to call the container:
```
docker build -t <IMAGE_NAME>:<TAG_NAME> .
```
6. To deploy the container you can use this command:
```
docker run --network=host <IMAGE_NAME>:<TAG_NAME> -p <CLOUD_CONTAINER_PORT>
```
***Note: If you are streaming video from Apsara you can use: "-ls <APSARA_URL>"***

***Note: Ensure that you use the same port number you are using with the application container option "-csp localhost:<port>"***  

### Setup the Cloud monitor Dashboard on Alibaba cloud.
1. Sign-in to your Alibaba cloud account and head to "CLoud Monitor" page
2. On the left side, click "Dashboard"
3. On the "Custom Dashboards" tab, Add a new dashboard
4. Enter a dashboard name and then click confirm
5. Go onto your custom dashboard then click "Add View"
6. Go onto "Custom Monitoring", change the "chart name" as "metric name". 
7. In custom monitoring, select metric name from the dropdown list at bottom as "<GROUP_ID>/<METRIC_NAME>". This is the same group id you used with the cloud container steps. Click "OK" to save the chart

***Note: I've had the best luck with visualing the data using the line graph type***