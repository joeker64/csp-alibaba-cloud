
# Building and Deploying the Cloud container

## Clone the repository

- Clone the gitlab repo using below command
```
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```
## Setup the Apsara Live service on Alibaba Cloud
- Refer to document docs/ARM-Smart_camera_CSP_Alibaba_ApasraVideo_Live.pdf for setting the Apsara live and VOD service for Live streaming and recording.

## Setup the container
- Execute the command on the target device(Rockchip RK3399 
or Imx8MP).
```
$ cd cloud_container/
```
- Open the config file located at config/service_config.json in an editor.
- The Device key and Product key will be same as the one used to setup Link IoT Edge service on the device. 
- Go to the Alibaba cloud and search for IoT platform on search bar.
- Click on the IoT platform from search results.
- Select the preferred region and device which was used to deploy Link IoT Edge on the local device.
- In the DeviceSecret click on view.
- Copy the Product Key and Device name.
- Update the copied credentials in the config file which is opened.
- For the Cloud monitor group id assign one number.
- Save the config file.
- Open another config file alibaba_creds.json located in config/ folder.
- In the config file update the region, access_key and secret_key.
- Save the file.
- Open another config file live_recording_config.json located in config/ folder.
- In the config file update the app_name, domain, stream_name, transcoding_id and max_cycle_duration.
- Save the file.

## Building the docker container
- Change the working directory to cloud contianer folder
```
$ cd cloud_container/
```
- Build the docker container
```
$ docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
```
- Update IMAGE_NAME and IMAGE_TAG. These can be given according to user choice eg. cloud:i3

## Running the docker container with usb webcam
- Run the docker contaienr
```
$ docker run --network=host <IMAGE_NAME>:<TAG_NAME> -p <CLOUD_CONTAINER_PORT> -ls <LIVE_STREAMING_PUSH_URL_FROM_APSARA_VIDEO_LIVE>
``` 
- Arguments :
  - -p: Port number on which the flask server will be running. Must be different from other containers.
  - -ls : Live streaming ingest URL from Apsara Live service.

## What's next, 
- Refer to Cloud Monitor folder. 
  -  ARM-Smart_camera_CSP_Alibaba_Setting_up_Cloud_Monitor.pdf