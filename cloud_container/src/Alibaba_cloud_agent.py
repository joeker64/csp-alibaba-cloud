import json
from uuid import uuid4
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest
from aliyunsdkcms.request.v20190101.PutCustomMetricRequest import PutCustomMetricRequest

from src.send_metrics_to_cloud import send_metrics_to_cloud
from src.recording_requests import VodLiveRecording
from src.apsara_vod_utils import Apsara_vod_utils


class AlibabaCloudAgent:
    def __init__(self):
        self.load_alibaba_creds()
        self.load_service_config()
        self.load_live_config()
        self.create_cloud_monitor_request()
        self.create_iot_request()
        self.default_client = self.get_default_clent()
        self.vod_client = self.get_vod_client()
        self.recording_client = VodLiveRecording(self.vod_client, self.live_rec_config["domain"], self.live_rec_config["app_name"], 
                                                 self.live_rec_config["stream_name"], self.live_rec_config["transcoding_id"], 
                                                 self.live_rec_config["max_cycle_duration"])
        self.apsara_client =  Apsara_vod_utils(self.vod_client)
        
    def load_live_config(self):
        live_rec_config = open("config/live_recording_config.json")
        self.live_rec_config = json.load(live_rec_config)
        
    def load_service_config(self):
        service_config = open("config/service_config.json")
        self.service_config = json.load(service_config)  

    def load_alibaba_creds(self):
        ali_creds = open("config/alibaba_creds.json")
        self.ali_creds = json.load(ali_creds)  

    def get_client(self, region):
        return AcsClient(self.ali_creds["access_key"], self.ali_creds["secret_key"], region, timeout=100000000)

    def get_vod_client(self):
        return self.get_client(self.service_config["vod_region"])
    
    def get_default_clent(self):
        return self.get_client(self.ali_creds["region"])
    
    def create_cloud_monitor_request(self):
        self.cm_req = PutCustomMetricRequest()                                                                                                           
        self.cm_req.set_accept_format('json')

    def create_iot_request(self):
        self.iot_req = CommonRequest()
        self.iot_req.set_accept_format('json')
        self.iot_req.set_domain('iot.ap-southeast-1.aliyuncs.com')
        self.iot_req.set_method('POST')
        self.iot_req.set_protocol_type('https') 
        self.iot_req.set_version('2018-01-20')
        self.iot_req.set_action_name('SetDeviceProperty')
        self.iot_req.add_query_param('RegionId', "ap-southeast1")
        self.iot_req.add_query_param('ProductKey', self.service_config["Product_key"])
        self.iot_req.add_query_param('DeviceName', self.service_config["Device_name"])
    
    def send_metrics(self, metrics):
        send_metrics_to_cloud(metrics, self.cm_req, self.iot_req, self.default_client, self.service_config["cloud_monitor_group_id"])

